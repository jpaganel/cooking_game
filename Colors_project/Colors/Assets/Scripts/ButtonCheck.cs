﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonCheck : MonoBehaviour
{
    public Text score_text;
    public Text game_over_score;

    public int button_number;
    int correct_button_number;

    public GameObject switcher;
    public GameObject game_over_menu;
    public GameObject menu;
    public Image timer;

    public void Check_button()
    {
        correct_button_number = switcher.GetComponent<Switcher>().correct_button;
        if (button_number == correct_button_number)
        {
            if (menu.GetComponent<Menu>().timer >= 1f)
            {
                menu.GetComponent<Menu>().timer -= 0.2f;
            }
            switcher.GetComponent<Switcher>().score += 1;
            switcher.GetComponent<Switcher>().Next_color();
            timer.fillAmount = 1;
            score_text.text = switcher.GetComponent<Switcher>().score.ToString();
        }
        else
        {
            menu.GetComponent<Menu>().timer = 0;
        }
    }
}
