﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public GameObject start_menu;
    public GameObject game_over_menu;
    public Text game_over_score;
    public Text best_score_text;
    public GameObject switcher;
    public int score;
    public int best_score = 1;

    public Image timer_image;
    public float timer = 10;

    void Start()
    {
        Time.timeScale = 0;
        best_score = PlayerPrefs.GetInt("setrecord");
    }

    void Update()
    {
        if (timer_image.fillAmount > 0)
        {
            timer_image.fillAmount -= Time.deltaTime / timer;
        }
        else
        {
            Time.timeScale = 0;
            game_over_menu.SetActive(true);
            score = switcher.GetComponent<Switcher>().score;
            game_over_score.text = score.ToString();

            if (score > best_score)
            {
                PlayerPrefs.SetInt("setrecord", score);
                PlayerPrefs.Save();
            }

            best_score = PlayerPrefs.GetInt("setrecord");
            best_score_text.text = best_score.ToString();
            
        }
    }

    public void Play()
    {
        start_menu.SetActive(false);
        Time.timeScale = 1;
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
