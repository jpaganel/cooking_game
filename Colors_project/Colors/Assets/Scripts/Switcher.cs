﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Switcher : MonoBehaviour
{
    Color[] my_color = new Color[]
    {
            new Color32(255, 0, 0, 255), // 1 Red
            new Color32(0, 0, 255, 255), // 2 Blue
            new Color32(0, 255, 0, 255), // 3 Green
            new Color32(255, 255, 0, 255), // 4 Yellow
            new Color32(0, 255, 255, 255), // 5 Cyan
            new Color32(255, 0, 255, 255), // 6 Magenta
            new Color32(255, 255, 255, 255), // 7 White
            new Color32(128, 127, 128, 255), // 8 Gray
            new Color32(139, 69, 19, 255), // 9 Brown
            new Color32(128, 0, 128, 255), // 10 Purple
            new Color32(218, 112, 214, 255), // 11 Pink
            new Color32(255, 69, 0, 255), // 12 Orange
            new Color32(128, 128, 0, 255) // 13 Olive
    };
    public Text main_textbox;

    public GameObject[] buttons;

    int color;
    int text_color;
    int button_color;
    public int correct_button;
    int button_with_text_color;

    public int score = 0;

    
    void Start()
    {
        Next_color();
    }


    public void Next_color()
    {
        color = Random.Range(1, 14);
        correct_button = Random.Range(0, 4);
        switch(color)
        {
            case 1:// Red
                main_textbox.text = "Red"; 
                buttons[correct_button].GetComponent<Image>().color = my_color[0];
                break;

            case 2:// Blue
                main_textbox.text = "Blue";
                buttons[correct_button].GetComponent<Image>().color = my_color[1];
                break;

            case 3:// Green
                main_textbox.text = "Green";
                buttons[correct_button].GetComponent<Image>().color = my_color[2];
                break;

            case 4:// Yellow
                main_textbox.text = "Yellow";
                buttons[correct_button].GetComponent<Image>().color = my_color[3];
                break;

            case 5:// Cyan
                main_textbox.text = "Cyan";
                buttons[correct_button].GetComponent<Image>().color = my_color[4];
                break;

            case 6:// Magenta
                main_textbox.text = "Magenta";
                buttons[correct_button].GetComponent<Image>().color = my_color[5];
                break;

            case 7:// White
                main_textbox.text = "White";
                buttons[correct_button].GetComponent<Image>().color = my_color[6];
                break;

            case 8:// Gray
                main_textbox.text = "Gray";
                buttons[correct_button].GetComponent<Image>().color = my_color[7];
                break;

            case 9:// Brown
                main_textbox.text = "Brown";
                buttons[correct_button].GetComponent<Image>().color = my_color[8];
                break;

            case 10:// Purple
                main_textbox.text = "Purple";
                buttons[correct_button].GetComponent<Image>().color = my_color[9];
                break;

            case 11:// Pink
                main_textbox.text = "Pink";
                buttons[correct_button].GetComponent<Image>().color = my_color[10];
                break;

            case 12:// Orange
                main_textbox.text = "Orange";
                buttons[correct_button].GetComponent<Image>().color = my_color[11];
                break;

            case 13:// Olive
                main_textbox.text = "Olive";
                buttons[correct_button].GetComponent<Image>().color = my_color[12];
                break;
        }
        Debug.Log("Правильная кнопка - " + (correct_button + 1));

        text_color = Random.Range(1, 14);
        while (color == text_color)
        {
            text_color = Random.Range(1, 14);
        }
        main_textbox.color = my_color[text_color - 1];
        Debug.Log("Текст - " + (text_color + 1));

        button_with_text_color = Random.Range(0, 4);
        while (button_with_text_color == correct_button)
        {
            button_with_text_color = Random.Range(0, 4);
        }
        buttons[button_with_text_color].GetComponent<Image>().color = main_textbox.color;
        Debug.Log("Номер кнопки с цветом как у текста - " + (button_with_text_color + 1));

        for (int i = 0; i < 4; i++)
        {
            if (i != correct_button && i != button_with_text_color)
            {
                button_color = Random.Range(1, 14);
                while (button_color == color || button_color == text_color)
                {
                    button_color = Random.Range(1, 14);
                }
                buttons[i].GetComponent<Image>().color = my_color[button_color - 1];
            }
        }
    }
}
