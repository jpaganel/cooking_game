﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    bool paused;
    public GameObject start_menu;
    public GameObject pause_menu;
    public GameObject win_panel;
    public GameObject lose_panel;

    public Text lose;
    public Text win;

    [SerializeField]
    Game_status game_status;

    void Start()
    {
        paused = true;
        Time.timeScale = 0;
    }
    
    void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Pause();
        }
        if (game_status.customers_quantity == 0)
        {
            if (game_status.orders_given >= (game_status.orders_quantity - 2))
            {
                win_panel.SetActive(true);
                win.text = game_status.orders_given.ToString() + "/" + (game_status.orders_quantity - 2).ToString();
                Time.timeScale = 0;
            }
            else
            {
                lose_panel.SetActive(true);
                lose.text = game_status.orders_given.ToString() + "/" + (game_status.orders_quantity - 2).ToString();
                Time.timeScale = 0;
            }
        }
    }

    public void Play()
    {
        start_menu.SetActive(false);
        pause_menu.SetActive(false);
        paused = false;
        Time.timeScale = 1;
    }

    public void Pause()
    {
        if (!paused)
        {
            pause_menu.SetActive(true);
            paused = true;
            Time.timeScale = 0;
        }
        else
        {
            pause_menu.SetActive(false);
            paused = false;
            Time.timeScale = 1;
        }
           
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
