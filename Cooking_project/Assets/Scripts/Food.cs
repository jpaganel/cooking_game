﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour
{
    [SerializeField]
    Game_status game_status;

    public float time; // time, added to customer waiting time

    void Start()
    {
        game_status = GameObject.Find("Game status").GetComponent<Game_status>();
        game_status.orders_list.Add(gameObject);
    }

    void OnDisable()
    {
        gameObject.transform.parent.parent.parent.GetComponent<Customer_Order>().progress_bar_current_amount += time;
        game_status.orders_list.Remove(gameObject);
	}
        
}
