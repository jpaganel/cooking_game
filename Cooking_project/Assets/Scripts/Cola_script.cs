﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cola_script : MonoBehaviour
{
	public Transform[] cola_spawn_positions;
	public GameObject cola;
	float timer = 0;
    void Start()
    {
       AddCola(); 
    }

    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= 5)
        {
			AddCola();
		}
    }
    
    public void AddCola()
    {
        for (int i = 0; i < cola_spawn_positions.Length; i++)
       	{
			if (cola_spawn_positions[i].transform.childCount == 0)
			{
				Instantiate (cola, cola_spawn_positions[i].transform);
                timer = 0;
                break;
			}
		}
	}
}
