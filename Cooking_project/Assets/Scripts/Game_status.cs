﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game_status : MonoBehaviour
{
    public enum Plates_status { empty, bread, cooked_dish};
    public enum Pans_status { empty, taken};
    public GameObject[] burger_plates;
    public GameObject[] hot_dog_plates;
    public GameObject[] burger_pans;
    public GameObject[] hot_dog_pans;

    public Plates_status[] burger_plates_status = new Plates_status[] { Plates_status.empty, };
    public Plates_status[] hot_dog_plates_status = new Plates_status[] { Plates_status.empty, };
    public Pans_status[] burger_pans_status = new Pans_status[] { Pans_status.empty, };
    public Pans_status[] hot_dog_pans_status = new Pans_status[] { Pans_status.empty, };

    public List<GameObject> customers_list = new List<GameObject>();
    public List<int> number_of_orders = new List<int>(); //1, 2 or 3 dishes in customer order
    public List<GameObject> orders_list = new List<GameObject>(); // List of  customer's orders on screen

    public GameObject[] customers_prefabs;
    public Transform[] customers_spawn_positions;
    public int[] customers_spawn_positions_status = { 0, 0, 0, 0 };
    
    public int orders_quantity;
    public int orders_given;
    public int customers_quantity;

    public Text orders_given_txt;
    public Text customers_left_txt;
    public Text goal_txt;

    public Image goal_bar;

    private void Start()
    {
        orders_given = 0;
        for (int i = 0; i < customers_quantity; i++)
        {
            customers_list.Add(customers_prefabs[Random.Range(0, customers_prefabs.Length)]);
            number_of_orders.Add(Random.Range(1, 4));
            orders_quantity += number_of_orders[number_of_orders.Count - 1];
        }
        goal_txt.text = "Цель - " + (orders_quantity - 2).ToString();
        customers_left_txt.text = customers_quantity.ToString();
        orders_given_txt.text = orders_given.ToString() + "/" + (orders_quantity - 2f).ToString();
    }
}
