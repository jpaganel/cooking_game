﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Add_Ingredient : MonoBehaviour
{
    public GameObject ingredient;
    public int position_number;

    [SerializeField]
    public Game_status gameStatus;

    public void Add()
    {
        switch(ingredient.name)
        {
            case "Raw_meat":
                if (position_number != 0)
                {
                    if (gameStatus.burger_pans_status[position_number - 1] == Game_status.Pans_status.empty)
                    {
                        Instantiate(ingredient, gameStatus.burger_pans[position_number - 1].transform);
                        gameStatus.burger_pans_status[position_number - 1] = Game_status.Pans_status.taken;
                    }
                }
                else
                {
                    for (int i = 0; i < gameStatus.burger_pans.Length; i++)
                    {
                        if (gameStatus.burger_pans_status[i] == Game_status.Pans_status.empty)
                        {
                            Instantiate(ingredient, gameStatus.burger_pans[i].transform);
                            gameStatus.burger_pans_status[i] = Game_status.Pans_status.taken;
                            break;
                        }
                    }
                }
                break;

            case "Raw_sausage":
                if (position_number != 0)
                {
                    if (gameStatus.hot_dog_pans_status[position_number - 1] == Game_status.Pans_status.empty)
                    {
                        Instantiate(ingredient, gameStatus.hot_dog_pans[position_number - 1].transform);
                        gameStatus.hot_dog_pans_status[position_number - 1] = Game_status.Pans_status.taken;
                    }
                }
                else
                {
                    for (int i = 0; i < gameStatus.hot_dog_pans.Length; i++)
                    {
                        if (gameStatus.hot_dog_pans_status[i] == Game_status.Pans_status.empty)
                        {
                            Instantiate(ingredient, gameStatus.hot_dog_pans[i].transform);
                            gameStatus.hot_dog_pans_status[i] = Game_status.Pans_status.taken;
                            break;
                        }
                    }
                }
                break;

            case "Burger_bread":
                if (position_number != 0)
                {
                    if (gameStatus.burger_plates_status[position_number - 1] == Game_status.Plates_status.empty)
                    {
                        Instantiate(ingredient, gameStatus.burger_plates[position_number - 1].transform);
                        gameStatus.burger_plates_status[position_number - 1] = Game_status.Plates_status.bread;
                    }
                }
                else
                {
                    for (int i = 0; i < gameStatus.burger_plates.Length; i++)
                    {
                        if (gameStatus.burger_plates_status[i] == Game_status.Plates_status.empty)
                        {
                            Instantiate(ingredient, gameStatus.burger_plates[i].transform);
                            gameStatus.burger_plates_status[i] = Game_status.Plates_status.bread;
                            break;
                        }
                    }
                }
                break;

            case "Hot_dog_bread":
                if (position_number != 0)
                {
                    if (gameStatus.hot_dog_plates_status[position_number - 1] == Game_status.Plates_status.empty)
                    {
                        Instantiate(ingredient, gameStatus.hot_dog_plates[position_number - 1].transform);
                        gameStatus.hot_dog_plates_status[position_number - 1] = Game_status.Plates_status.bread;
                    }
                }
                else
                {
                    for (int i = 0; i < gameStatus.hot_dog_plates.Length; i++)
                    {
                        if (gameStatus.hot_dog_plates_status[i] == Game_status.Plates_status.empty)
                        {
                            Instantiate(ingredient, gameStatus.hot_dog_plates[i].transform);
                            gameStatus.hot_dog_plates_status[i] = Game_status.Plates_status.bread;
                            break;
                        }
                    }
                }
                break;
        }
	}
}