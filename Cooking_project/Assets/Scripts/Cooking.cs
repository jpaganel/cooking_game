﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cooking : MonoBehaviour
{
    float timer = 0;
	public GameObject ingredient;

    int parent_position_number;
    
    [SerializeField]
    Game_status game_status;

    private void Start()
    {
        game_status = GameObject.Find("Game status").GetComponent<Game_status>();
        parent_position_number = gameObject.GetComponentInParent<Add_Ingredient>().position_number;
    }


    void Update()
    {
      timer += Time.deltaTime; 
    }
    
    public void CookingReady()
    {
        if (timer > 5 && timer < 12)
        {
            switch (ingredient.name)
            {
                case "Burger":
                    for (int i = 0; i < game_status.burger_plates.Length; i++)
                    {
                        if (game_status.burger_plates_status[i] == Game_status.Plates_status.bread)
                        {
                            Destroy(game_status.burger_plates[i].transform.GetChild(0).gameObject);
                            Instantiate(ingredient, game_status.burger_plates[i].transform);
                            game_status.burger_plates_status[i] = Game_status.Plates_status.cooked_dish;
                            game_status.burger_pans_status[parent_position_number - 1] = 0;
                            Destroy(gameObject);
                            break;
                        }
                    }
                    break;

                case "Hot_dog":
                    for (int i = 0; i < game_status.hot_dog_plates.Length; i++)
                    {
                        if (game_status.hot_dog_plates_status[i] == Game_status.Plates_status.bread)
                        {
                            Destroy(game_status.hot_dog_plates[i].transform.GetChild(0).gameObject);
                            Instantiate(ingredient, game_status.hot_dog_plates[i].transform);
                            game_status.hot_dog_plates_status[i] = Game_status.Plates_status.cooked_dish;
                            game_status.hot_dog_pans_status[parent_position_number - 1] = 0;
                            Destroy(gameObject);
                            break;
                        }
                    }
                    break;
            }
        }
	}

    public void CookingBurn()
    {
        switch(ingredient.name)
        {
            case "Burger":
                if (timer > 12)
                {
                    game_status.burger_pans_status[parent_position_number - 1] = Game_status.Pans_status.empty;
                    Destroy(gameObject);
                }
                break;

            case "Hot_dog":
                if (timer > 12)
                {
                    game_status.hot_dog_pans_status[parent_position_number - 1] = Game_status.Pans_status.empty;
                    Destroy(gameObject);
                }
                break;
        }
	}
}
