﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Customer_Spawn : MonoBehaviour
{
    [SerializeField]
    Game_status game_status;

    GameObject customer_clone;

    public float timer = 0;

    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= 3 && game_status.customers_list.Any())
        {
			SpawnCustomer();
		}
    }
    
    public void SpawnCustomer()
    {
		timer = 0;
		for (int i = 0; i < game_status.customers_spawn_positions.Length; i++)
       	{
			if (game_status.customers_spawn_positions_status[i] == 0)
			{
                customer_clone = Instantiate (game_status.customers_list[0], game_status.customers_spawn_positions[i].transform);
                customer_clone.GetComponent<Customer_Order>().orders_quantity = game_status.number_of_orders[game_status.number_of_orders.Count - 1];
                customer_clone.GetComponent<Customer_Order>().customer_place_number = i;
                game_status.number_of_orders.RemoveAt(game_status.number_of_orders.Count - 1);
                game_status.customers_list.RemoveAt(0);
                game_status.customers_spawn_positions_status[i] = 1;
                break;
			}
		}
	}
}
