﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Customer_Order : MonoBehaviour
{
    Game_status game_status;

    [SerializeField]
    GameObject[] orders_spawn_points;
    [SerializeField]
    GameObject[] orders;
    public int orders_quantity;

    public Image customer_progress_bar;
    public float progress_bar_current_amount = 1;
    public float customer_waiting_time = 18;
    public int customer_place_number;

    Customer_Spawn respawn_timer;

    bool all_orders_are_given;

    void Start()
    {
        game_status = GameObject.Find("Game status").GetComponent<Game_status>();
        respawn_timer = GameObject.Find("Spawner").GetComponent<Customer_Spawn>();
        Debug.Log(orders_quantity);
        all_orders_are_given = false;
        for (int i = 0; i < orders_quantity; i++)
        {
            Instantiate(orders[Random.Range(0, orders.Length)], orders_spawn_points[i].transform);
        }
    }

    void Update()
    {
        if (progress_bar_current_amount > 1)
        {
            progress_bar_current_amount = 1;
        }
        if (progress_bar_current_amount > 0)
        {
            progress_bar_current_amount -= Time.deltaTime / customer_waiting_time;
        }
        customer_progress_bar.fillAmount = progress_bar_current_amount;
        if (progress_bar_current_amount <= 0)
        {
            CustomerGone();
        }
    }

    void LateUpdate()
    {
        all_orders_are_given = true;
        for (int i = 0; i < orders_spawn_points.Length; i++)
        {
            if (orders_spawn_points[i].transform.childCount > 0)
            {
                all_orders_are_given = false;
                break;
            }
        }
        if (all_orders_are_given == true)
        {
            CustomerGone();
        }
    }

    void CustomerGone()
    {
        respawn_timer.timer = 0;
        game_status.customers_spawn_positions_status[customer_place_number] = 0;
        game_status.customers_quantity -= 1;
        game_status.customers_left_txt.text = game_status.customers_quantity.ToString();
        Destroy(gameObject);
    }
}
