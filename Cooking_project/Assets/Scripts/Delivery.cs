﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Delivery : MonoBehaviour
{
    [SerializeField]
    Game_status game_status;

    float customer_waiting_time;
    int parent_position_number;
    
    float time;
    int index_of_order; //index of order with right name and least time

    void Start()
    {
        game_status = GameObject.Find("Game status").GetComponent<Game_status>();
    }
    
    public void Deliver()
    {
        time = 2;
        index_of_order = -1;
		foreach(GameObject order in game_status.orders_list)
        {
            if (order.CompareTag(gameObject.tag))
            {
                customer_waiting_time = order.transform.parent.parent.parent.gameObject.GetComponent<Customer_Order>().progress_bar_current_amount;
                if (customer_waiting_time < time)
                {
                    time = customer_waiting_time;
                    index_of_order = game_status.orders_list.IndexOf(order);
                }
            }
        }
        if (index_of_order >= 0)
        {
            Destroy(game_status.orders_list[index_of_order]);
            switch (gameObject.tag)
            {
                case "burger":
                    parent_position_number = gameObject.transform.parent.GetComponent<Add_Ingredient>().position_number;
                    game_status.burger_plates_status[parent_position_number - 1] = 0;
                    break;
                case "hot_dog":
                    parent_position_number = gameObject.transform.parent.GetComponent<Add_Ingredient>().position_number;
                    game_status.hot_dog_plates_status[parent_position_number - 1] = 0;
                    break;
            }
            game_status.orders_given += 1;
            game_status.orders_given_txt.text = game_status.orders_given.ToString() + "/" + (game_status.orders_quantity - 2).ToString();
            game_status.goal_bar.fillAmount += 1 / ((float) game_status.orders_quantity - 2);
            if (gameObject.tag == "cola")
            {
                Destroy(gameObject.transform.parent.gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }
	}
}
