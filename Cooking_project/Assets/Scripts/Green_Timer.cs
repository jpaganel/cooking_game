﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Green_Timer : MonoBehaviour
{
    public GameObject swap_object;
    public Image loading_bar;
    
    public float timer;
	float current_amount = 0;

    void Update()
    {
        if (current_amount < 1)
        {
            current_amount += Time.deltaTime / timer;
		}
        loading_bar.fillAmount = current_amount;
		if (current_amount >= 1)
		{
			if (swap_object != null)
			{
                swap_object.SetActive(true);
			}
			Destroy (gameObject);
		}
    }
}
