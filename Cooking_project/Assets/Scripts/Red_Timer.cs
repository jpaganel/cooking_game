﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Red_Timer : MonoBehaviour
{
    [SerializeField]
	public Transform loadingBar;

    [SerializeField]
	public float timer;
	float currentAmount = 0;

    void Update()
    {
        if (currentAmount < 1)
        {
			currentAmount += Time.deltaTime / timer;
		}
        loadingBar.GetComponent<Image>().fillAmount = currentAmount;
		if (currentAmount >= 1)
		{
			Destroy (gameObject);
		}
    }
}
